@extends('layouts.app')

@section('content')
<div class="section home-content-overview post" id="home-content-overview">
    <div class="overview-btm-left">
        <img src="{{asset('assets/images/book.png')}}" alt="" class="img-responsive">
    </div>
    <div class="overview-btm-right">
        <h3 class="title">Prima Official Game Guide</h3>
        <h4 class="description">David Knight, author of numerous Prima Official Game Guides, offers his readers yet another complete guide to another best-selling game: <strong class="txt-green">Dragon Age Inquisition.</strong></h4>
    </div>
</div>
<div class="section home-content-intro post" id="home-content-intro">
    <div class="inner">
        <div class="home-intro">
            <img src="{{asset('assets/images/intro.png')}}" alt="" class="img-responsive">
            <h6>Claim the glory</h6>
            <a class="btn btn-primary">Hardcover<small>$16.20</small></a>
            <a class="btn btn-primary">Paperback<small>$16.20</small></a>
            <p>It is more than just a game. In Dragon Age Inquisition, the players are given the opportunity to lead the Inquisition and pursuit the instruments of chaos. The restoration of order depends upon the player and his trusted allies. The player needs to make tough and wise decisions that will determine the success of his exploration, leadership, and battles. Every decision counts as each one can alter the path towards the main goal.</p>
        </div>
        <div class="author-content">
            <img src="{{asset('assets/images/author.png')}}" alt="">
            <h3 class="author-line">“Become a pro in no time!”</h3>
            <h5 class="author-names">– Ben Templeton</h5>
        </div>
    </div>
</div>
<div class="sword">
    <img src="{{asset('assets/images/sword.png')}}" alt="" class="img-responsive">
</div>
<div class="section home-content-intro post" id="home-content-intro">
    <div class="inner">
        <div class="intro-columns">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-responsive">
                    <h4 class="sub-title">Become the best inquisitor</h4>
                    <h5>Choose your class wisely with the use of character recommendations and learn how to make the most out of an outstanding set of skills, special moves, and weaponry, and how to get full control of the  appearance and skills.</h5>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-responsive">
                    <h4 class="sub-title">Make the right choices</h4>
                    <h5>Build a better world within the game through learning how to make the right choices and do the right actions as these will affect the story and the game’s physical aspects. One tough choice can lead to the  improvement or the downfall of your game.</h5>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-responsive">
                    <h4 class="sub-title">Maximize the potential of your team</h4>
                    <h5>Build strong relationships with your party and your allies by connecting and communicating with other characters. Developing a strong and dynamic relationship with other characters will be beneficial for both parties.</h5>
                </div>
            </div>
            <div class="author-content">
                <img src="{{asset('assets/images/author.png')}}" alt="">
                <h3 class="author-line">“I’ve made the right decision of buying this guide.”</h3>
                <h5 class="author-names">– James Williams</h5>
            </div>
        </div>
    </div>
</div>
<div class="section home-content-benefits post" id="home-content-benefits">
    <div class="row">
        <div class="col-xs-12 col-md-push-5 col-md-6">
            <div class="home-benefits-right">
                <h1 class="sub-title">What’s In Store for You?</h1>
                <h5 class="description"><strong class="txt-green">Dragon Age Inquisition: Prima Official Game Guide</strong> has over 300 pages of guides and strategies that will  help the players be the best that they can be.</h5>
                <h4 class="sub-title"><strong class="description">Here are some the things to look out for in this guide:</strong></h4>
                <ul class="guide list-unstyled">
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn useful tactics</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn about the hidden caves waiting to be explored</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn how to defeat huge creatures</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn how to build the world around you</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn how to make the right decisions</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn how to be the best inquisitor</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn your way around the game through the illustrated maps</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn about the various areas in the maps</p></li>
                    <li><img src="{{asset('assets/images/bullet.ico')}}" alt="">
                        <p>Learn about operations and outcomes</p></li>
                </ul>
                <p class="desc">Dragon Age inquisition is an action-packed RPG that revolves around “saving the world from itself”, as the game’s creative director, Mike Laidlaw stated. It is set in the continent of Thedas, as with the other games in the franchise. Inquisition is the third installment of the Dragon Age franchise, following Dragon Age: Origins and Dragon Age II. The area covered in Inquisition is much larger than the two previous games which is why there is a need for a guide that contains maps that will help the player navigate in the game. Through maps, the player can easily know his way around Thedas and accomplish goals easily.</p>
                <p>To know more about the game, go to <strong class="txt-green">www.dragonage.com.</strong></p>
            </div>
        </div>
        <div class="col-xs-12 col-md-pull-6 col-md-5">
            <div class="home-benefits-left">
                <img src="{{asset('assets/images/benefits.png')}}" alt="" class="img-responsive">
            </div>
            <div class="author-content text-center">
                <img src="{{asset('assets/images/author.png')}}" alt="">
                <h3 class="author-line">“I learned how to play the game as easy as 1-2-3!”</h3>
                <h5 class="author-names">- Toby Brown</h5>
            </div>
        </div>
    </div>
</div>
<div class="section home-content-reviews post" id="home-content-reviews">
    <div class="inner">
        <div class="text-center">
            <h1 class="title">Customer Reviews</h1>
            <h4>Here’s what their saying about this book</h4>
            <p>Read about what the gamers had to say about <strong class="txt-green">Dragon Age Inquisition: Prima Official Game Guide.</strong></p>
        </div>
        <div class="reviews-mdl">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-md-offset-1 col-md-2">
                            <img src="{{asset('assets/images/small-circle.png')}}" alt="">
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <strong class="h4">Han Li</strong>
                            <h5>“For me, Dragon Age Inquisition is not just a game. It’s more than that. It teaches the players a lot of skills that we can use in real life such as decision making skills and leadership skills. This book is the quickest guide to maximizing the game’s potential and reaping benefits out of it.”</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-md-offset-1 col-md-2">
                            <img src="{{asset('assets/images/small-circle.png')}}" alt="">
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <strong class="h4">Bryan White</strong>
                            <h5>“This is great guide for decision making, maps, romances, and other aspects of the storyline. It also has details on how to shape the world around you in the game. However, it is somehow lacking information about where to find mosaic pieces and crafting recipes, but in other aspects it works just fine.”</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-md-offset-1 col-md-2">
                            <img src="{{asset('assets/images/small-circle.png')}}" alt="">
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <strong class="h4">Thomas Max</strong>
                            <h5>“It is a very interesting guide. Its strong points are the maps and information about different areas, class skill trees, and useful information on each zone’s agent of chaos. The only problem I have with this guide is the lack of details on where to find crafting recipes. Other than that, I find this guide one of the best guides for Dragon Age Inquisition so far.”</h5>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="row">
                        <div class="col-sm-3 col-md-offset-1 col-md-2">
                            <img src="{{asset('assets/images/small-circle.png')}}" alt="">
                        </div>
                        <div class="col-sm-9 col-md-9">
                            <strong class="h4">Gilbert Cyrus</strong>
                            <h5>“It is a must-have guide for those who are new to the game and for those who have been playing the game for quite some time as well. It has a lot of information about side quests and how to choose the right romantic interest. You may get dumped in real life but when you get this guide, you will never get dumped in the game… if that’s any consolation.”</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div class="reviews-btm">
                <strong class="h4">Call to action text and description goes here to intice buyers</strong>
                <a class="btn btn-primary">Buy now</a>
            </div>
            <div class="author-content">
                <img src="{{asset('assets/images/author.png')}}" alt="">
                <h3 class="author-line">“I learned how to play the game as easy as 1-2-3!”</h3>
                <h5 class="author-names">- Toby Brown</h5>  
            </div>
        </div>
    </div>
    <div class="download">
        <div class="text-center">
            <h2 class="download-top">Be the best inquisitor when you get a copy of Dragon Age Inquisition: Prima Official Game Guide</h2>
            <h4 class="download-mdl">This guide is available in paperback for only $15.81. You can also get the Collector’s Guide edition that includes additional pages on hardcover for only $24.15. What are you waiting for? Get a copy and add it to your collection!</h4>
            <button class="btn btn-primary">download now</button>
        </div>
    </div>
</div>
<div class="section home-content-author post" id="home-content-author">
    <div class="inner">
        <div class="content-author-top">
            <h1>About the Author</h1>
            <h3>Some facts and bio about the author</h3>
        </div>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

              <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-reponsive">
                </div>
                <div class="item">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-reponsive">
                </div>
                <div class="item">
                    <img src="{{asset('assets/images/circle.png')}}" alt="" class="img-reponsive">
                </div>
            </div>
        </div>
        <div class="content-author-mdl">
            <h3>David Knight</h3>
            <p>is a passionate gamer and the author of Prima Games. His love for video games bloomed at an early age, back when his gaming devices were still in the forms of NES and Atari 2600. His first debut in the gaming industry was in 1995, wherein he became a scenario designer for SSI’s Steel Panthers, a WWII strategy game. He also generously lent his skills in design to many online fan sites when online gaming communities started to resurface in the late 90’s.</p>
            <p>In 1998, David Knight co-founded and hosted a weekly webcast that featured game reviews and news  about the gaming industry. The webcast was called Game Waves. Also in the late 90’s, he got involved  with Prima Games as a technical editor. In 2000, he wrote his first game guide for Rollercoaster Tycoon 2 and since then, he has written of 50 game guides for Prima Games. To name a few, Knight had authored guides for popular games such as Left 4 Dead, Far Cry 2, Metroid Prime 3, SimCity, and Mario & Luigi: Dream Team.</p>
        </div>
        <div class="content-author-btm">
            <h4>You can find his best-selling game guides on Amazon.com</h4>
            <a class="btn btn-primary">CHECK OUT HIS BEST SELLING GAME GUIDES</a>
        </div>
    </div>
</div>
@endsection
