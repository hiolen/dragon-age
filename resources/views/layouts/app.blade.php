<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book LP</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,900italic,900,700italic,700,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>   
</head>
<body>
    <div class="wrap ">
        <div class="menu ">
            <div class="inner">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-menu" aria-expanded="false">
                        <i class="fa fa-bars"></i>
                    </button>
                    <div class="logo">
                        <img src="{{asset('assets/images/logo.png')}}" alt="Logo">
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="main-menu">
                    <ul class="nav navbar-nav main-menu">
                        <li><a href="#home-content-overview" class="subNavBtn">Overview</a> </li>
                        <li><a href="#home-content-intro" class="subNavBtn">Intro</a></li>
                        <li><a href="#home-content-benefits" class="subNavBtn">Benefits</a></li>
                        <li><a href="#home-content-reviews" class="subNavBtn">Reviews</a></li>
                        <li><a href="#home-content-author" class="subNavBtn">Author</a></li>
                    </ul>
                </div>  
                <a href="" class="btn btn-primary h6">download</a>
            </div>
        </div>

        @yield('content')

    </div>


    <script src="{{asset('assets/js/jquery.smint.js')}}"></script>
    <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>

    <script src="{{asset('assets/js/jquery-1.12.0.min.js')}}"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.smint.js')}}" type="text/javascript" ></script>
    <script src="{{asset('assets/js/script.js')}}"></script>

</body>
</html>
